// Generated code from Butter Knife. Do not modify!
package br.com.saulomiranda.desafio.adapter;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RepositoriesAdapter$ViewHolder$$ViewBinder<T extends br.com.saulomiranda.desafio.adapter.RepositoriesAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492987, "field 'nameRepository'");
    target.nameRepository = finder.castView(view, 2131492987, "field 'nameRepository'");
    view = finder.findRequiredView(source, 2131492988, "field 'description'");
    target.description = finder.castView(view, 2131492988, "field 'description'");
    view = finder.findRequiredView(source, 2131492989, "field 'forks'");
    target.forks = finder.castView(view, 2131492989, "field 'forks'");
    view = finder.findRequiredView(source, 2131492990, "field 'stars'");
    target.stars = finder.castView(view, 2131492990, "field 'stars'");
    view = finder.findRequiredView(source, 2131492992, "field 'username'");
    target.username = finder.castView(view, 2131492992, "field 'username'");
    view = finder.findRequiredView(source, 2131492991, "field 'profilePic'");
    target.profilePic = finder.castView(view, 2131492991, "field 'profilePic'");
  }

  @Override public void unbind(T target) {
    target.nameRepository = null;
    target.description = null;
    target.forks = null;
    target.stars = null;
    target.username = null;
    target.profilePic = null;
  }
}
