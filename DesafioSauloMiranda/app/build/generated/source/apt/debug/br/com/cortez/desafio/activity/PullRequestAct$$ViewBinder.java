// Generated code from Butter Knife. Do not modify!
package br.com.cortez.desafio.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PullRequestAct$$ViewBinder<T extends br.com.cortez.desafio.activity.PullRequestAct> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492972, "field 'recyclerView'");
    target.recyclerView = finder.castView(view, 2131492972, "field 'recyclerView'");
    view = finder.findRequiredView(source, 2131492971, "field 'swipeContainer'");
    target.swipeContainer = finder.castView(view, 2131492971, "field 'swipeContainer'");
    view = finder.findRequiredView(source, 2131492990, "field 'loading'");
    target.loading = view;
    view = finder.findRequiredView(source, 2131492982, "field 'error'");
    target.error = view;
  }

  @Override public void unbind(T target) {
    target.recyclerView = null;
    target.swipeContainer = null;
    target.loading = null;
    target.error = null;
  }
}
