// Generated code from Butter Knife. Do not modify!
package br.com.saulomiranda.desafio.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ListRepositoriesActivity$$ViewBinder<T extends br.com.saulomiranda.desafio.activity.ListRepositoriesActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492975, "field 'repositories'");
    target.repositories = finder.castView(view, 2131492975, "field 'repositories'");
    view = finder.findRequiredView(source, 2131492974, "field 'swipeContainer'");
    target.swipeContainer = finder.castView(view, 2131492974, "field 'swipeContainer'");
    view = finder.findRequiredView(source, 2131492993, "field 'loading'");
    target.loading = view;
    view = finder.findRequiredView(source, 2131492985, "field 'error'");
    target.error = view;
  }

  @Override public void unbind(T target) {
    target.repositories = null;
    target.swipeContainer = null;
    target.loading = null;
    target.error = null;
  }
}
