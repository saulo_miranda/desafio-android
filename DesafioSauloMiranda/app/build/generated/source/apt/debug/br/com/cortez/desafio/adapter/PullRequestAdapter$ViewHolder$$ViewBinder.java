// Generated code from Butter Knife. Do not modify!
package br.com.cortez.desafio.adapter;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PullRequestAdapter$ViewHolder$$ViewBinder<T extends br.com.cortez.desafio.adapter.PullRequestAdapter.ViewHolder> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131493005, "field 'name'");
    target.name = finder.castView(view, 2131493005, "field 'name'");
    view = finder.findRequiredView(source, 2131493006, "field 'description'");
    target.description = finder.castView(view, 2131493006, "field 'description'");
    view = finder.findRequiredView(source, 2131493007, "field 'date'");
    target.date = finder.castView(view, 2131493007, "field 'date'");
    view = finder.findRequiredView(source, 2131493009, "field 'username'");
    target.username = finder.castView(view, 2131493009, "field 'username'");
    view = finder.findRequiredView(source, 2131493008, "field 'profilePic'");
    target.profilePic = finder.castView(view, 2131493008, "field 'profilePic'");
  }

  @Override public void unbind(T target) {
    target.name = null;
    target.description = null;
    target.date = null;
    target.username = null;
    target.profilePic = null;
  }
}
