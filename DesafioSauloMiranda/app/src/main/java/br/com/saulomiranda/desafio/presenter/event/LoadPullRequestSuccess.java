package br.com.saulomiranda.desafio.presenter.event;

import java.util.List;

import br.com.saulomiranda.desafio.model.PullRequest;

/**
 * Created by Saulo on 25/9/16..
 */

public class LoadPullRequestSuccess {

    private List<PullRequest> pullRequests;

    public LoadPullRequestSuccess(List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
    }

    public List<PullRequest> getPullRequests() {
        return pullRequests;
    }
}
