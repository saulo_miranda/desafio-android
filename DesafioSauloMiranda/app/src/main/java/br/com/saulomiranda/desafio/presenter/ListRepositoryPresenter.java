package br.com.saulomiranda.desafio.presenter;

import com.squareup.otto.Subscribe;

import br.com.saulomiranda.desafio.DesafioApplication;
import br.com.saulomiranda.desafio.presenter.event.LoadFailed;
import br.com.saulomiranda.desafio.presenter.event.LoadListRepositorySuccess;
import br.com.saulomiranda.desafio.presenter.event.LoadMoreFailed;
import br.com.saulomiranda.desafio.service.GithubRepositoryService;
import br.com.saulomiranda.desafio.view.ListRepositoryView;


public class ListRepositoryPresenter {

    private ListRepositoryView view;
    private GithubRepositoryService service;

    public ListRepositoryPresenter(ListRepositoryView view) {
        this.view = view;
        this.service = DesafioApplication.getInstance().providesGithubRepositoryService();

    }

    public void start() {
        DesafioApplication.getInstance().providesBus().register(this);
    }

    public void stop() {
        DesafioApplication.getInstance().providesBus().unregister(this);
    }

    public void loadViews() {
        service.loadGithubRepositories();
    }

    @Subscribe
    public void onLoadRepositorySuccess(LoadListRepositorySuccess loadListRepositorySuccess) {
        view.showRepositories(loadListRepositorySuccess.getRepositories());
    }

    @Subscribe
    public void onLoadRepositoryError(LoadFailed loadFailed) {
        view.showError();
    }

    @Subscribe
    public void onLoadRepositoryError(LoadMoreFailed loadMoreRepositoriesFailed) {
        view.errorLoadMoreContent();
    }

    public void loadMoreRepositories() {
        service.loadMoreGithubRepositories();
    }

}
