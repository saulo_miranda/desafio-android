package br.com.saulomiranda.desafio.presenter.event;

import java.util.List;

import br.com.saulomiranda.desafio.model.Repository;

/**
 * Created by Saulo on 25/9/16.
 */

public class LoadListRepositorySuccess {

    private List<Repository> repositories;

    public LoadListRepositorySuccess(List<Repository> repositories) {
        this.repositories = repositories;
    }

    public List<Repository> getRepositories() {
        return repositories;
    }
}
