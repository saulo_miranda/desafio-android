package br.com.saulomiranda.desafio.repository.http;

/**
 * Created by Saulo on 25/9/16.
 */

public interface CallbackHttp<T> {

    void success(T param);

    void onError(int status, String error);
}
