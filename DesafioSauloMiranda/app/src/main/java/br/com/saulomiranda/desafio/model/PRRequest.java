package br.com.saulomiranda.desafio.model;

import java.util.List;

/**
 * Created by Saulo on 25/9/16..
 */

public class PRRequest {


    List<PullRequest> pullRequestList;

    public List<PullRequest> getPullRequestList() {
        return pullRequestList;
    }

    public void setPullRequestList(List<PullRequest> pullRequestList) {
        this.pullRequestList = pullRequestList;
    }
}
