package br.com.saulomiranda.desafio.repository;

import java.util.List;

import br.com.saulomiranda.desafio.model.Repository;
import br.com.saulomiranda.desafio.repository.http.CallbackHttp;

public interface GithubRepository {


    void getRespositories(int page, CallbackHttp<List<Repository>> callbackHttp);



}
