package br.com.saulomiranda.desafio.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.saulomiranda.desafio.DesafioApplication;
import br.com.saulomiranda.desafio.R;
import br.com.saulomiranda.desafio.imageLoader.ImageLoader;
import br.com.saulomiranda.desafio.model.PullRequest;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Saulo on 25/9/16.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.ViewHolder> {

    private List<PullRequest> pullRequests;
    private ImageLoader imageLoader;
    private WeakReference<Context> contextWeakReference;
    private PullRequestClickListener pullRequestClickListener;

    public PullRequestAdapter(PullRequestClickListener pullRequestClickListener) {
        this.pullRequestClickListener = pullRequestClickListener;
        this.pullRequests = new ArrayList<>();
        this.imageLoader = DesafioApplication.getInstance().providesImageLoader();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pull_requests_row, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PullRequest pullRequest = pullRequests.get(position);

        final String username = pullRequest.getUser().getLogin();
        String url = pullRequest.getUser().getAvatarUrl();
        int size = (int) getContext().getResources().getDimension(R.dimen.profile_size);


        holder.name.setText(pullRequest.getTitle());
        holder.description.setText(pullRequest.getDescription());
        holder.date.setText(formatStringData(pullRequest.getCreate()));
        holder.username.setText(username);

        imageLoader.load(url, holder.profilePic, size, size);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pullRequestClickListener.onItemClick(pullRequest.getUrl());
            }
        });

    }

    private String formatStringData(String data) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat formatString = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date = sdf.parse(data);
            return formatString.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public int getItemCount() {
        if (pullRequests == null) {
            return 0;
        }
        return pullRequests.size();
    }


    public void addItens(List<PullRequest> pullRequests) {
        this.pullRequests.addAll(pullRequests);
    }

    private Context getContext() {
        if (contextWeakReference == null) {
            contextWeakReference = new WeakReference<Context>(DesafioApplication.getInstance());
        }
        return contextWeakReference.get();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.pullRequest_name)
        TextView name;
        @Bind(R.id.pullRequest_description)
        TextView description;
        @Bind(R.id.pullRequest_date)
        TextView date;
        @Bind(R.id.pullRequest_username)
        TextView username;
        @Bind(R.id.pullRequest_profile)
        ImageView profilePic;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface PullRequestClickListener {

        void onItemClick(String url);
    }
}
