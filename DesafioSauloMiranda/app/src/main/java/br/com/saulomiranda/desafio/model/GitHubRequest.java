
package br.com.saulomiranda.desafio.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GitHubRequest {

    @SerializedName("total_count")
    public int totalCount;
    @SerializedName("incomplete_results")
    public boolean incompleteResults;
    @SerializedName("items")
    public List<Repository> items = new ArrayList<Repository>();

}
