package br.com.saulomiranda.desafio.presenter;

import com.squareup.otto.Subscribe;

import br.com.saulomiranda.desafio.DesafioApplication;
import br.com.saulomiranda.desafio.presenter.event.LoadFailed;
import br.com.saulomiranda.desafio.presenter.event.LoadPullRequestSuccess;
import br.com.saulomiranda.desafio.service.PullRequestService;
import br.com.saulomiranda.desafio.view.PullRequestView;

/**
 * Created by Saulo on 25/9/16..
 */

public class PullRequestPresenter {

    private PullRequestView view;
    private PullRequestService service;

    public PullRequestPresenter(PullRequestView pullRequestView) {
        this.view = pullRequestView;
        this.service = DesafioApplication.getInstance().providesPullRequestService();
    }

    public void start() {
        DesafioApplication.getInstance().providesBus().register(this);
    }

    public void stop() {
        DesafioApplication.getInstance().providesBus().unregister(this);
    }

    public void loadViews(String name, String repository) {
        view.showLoading();
        service.loadPullRequests(name, repository);
    }

    @Subscribe
    public void onLoadRepositorySuccess(LoadPullRequestSuccess loadPullRequestSuccess) {
        view.showPullRequests(loadPullRequestSuccess.getPullRequests());
    }

    @Subscribe
    public void onLoadRepositoryError(LoadFailed loadFailed) {
        view.showError();
    }


}
