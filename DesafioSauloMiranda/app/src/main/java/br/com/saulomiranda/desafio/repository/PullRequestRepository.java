package br.com.saulomiranda.desafio.repository;

import java.util.List;

import br.com.saulomiranda.desafio.model.PullRequest;
import br.com.saulomiranda.desafio.repository.http.CallbackHttp;

/**
 * Created by Saulo on 25/9/16..
 */

public interface PullRequestRepository {

    void getPullRequests(String name, String repository, CallbackHttp<List<PullRequest>> callbackHttp);
}
