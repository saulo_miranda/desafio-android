package br.com.saulomiranda.desafio.service;

/**
 * Created by Saulo on 25/9/16..
 */

public interface PullRequestService {

    void loadPullRequests(String name, String repository);

}
