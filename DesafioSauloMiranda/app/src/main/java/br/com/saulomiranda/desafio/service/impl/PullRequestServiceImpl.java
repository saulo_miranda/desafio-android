package br.com.saulomiranda.desafio.service.impl;

import java.util.List;

import br.com.saulomiranda.desafio.DesafioApplication;
import br.com.saulomiranda.desafio.model.PullRequest;
import br.com.saulomiranda.desafio.presenter.event.LoadFailed;
import br.com.saulomiranda.desafio.presenter.event.LoadPullRequestSuccess;
import br.com.saulomiranda.desafio.repository.PullRequestRepository;
import br.com.saulomiranda.desafio.repository.http.CallbackHttp;
import br.com.saulomiranda.desafio.service.PullRequestService;

/**
 * Created by Saulo on 25/9/16..
 */

public class PullRequestServiceImpl implements PullRequestService {


    private PullRequestRepository pullRequestRepository;

    CallbackHttp<List<PullRequest>> listCallbackHttp;

    public PullRequestServiceImpl(PullRequestRepository pullRequestRepository) {
        this.pullRequestRepository = pullRequestRepository;
        this.listCallbackHttp = getCallback();
    }

    @Override
    public void loadPullRequests(String name, String repository) {
        pullRequestRepository.getPullRequests(name, repository, listCallbackHttp);

    }

    private CallbackHttp<List<PullRequest>> getCallback() {
        return new CallbackHttp<List<PullRequest>>() {
            @Override
            public void success(List<PullRequest> param) {
                DesafioApplication.getInstance().providesBus().post(new LoadPullRequestSuccess(param));
            }

            @Override
            public void onError(int status, String error) {
                DesafioApplication.getInstance().providesBus().post(new LoadFailed());
            }
        };
    }
}
