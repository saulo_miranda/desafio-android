package br.com.saulomiranda.desafio.view;

import java.util.List;

import br.com.saulomiranda.desafio.model.Repository;

/**
 * Created by Saulo on 25/9/16.
 */

public interface ListRepositoryView {

    void showLoading();

    void hideLoading();

    void showRepositories(List<Repository> repositories);

    void showError();

    void errorLoadMoreContent();


}
