package br.com.saulomiranda.desafio.service.impl;

import java.util.List;

import br.com.saulomiranda.desafio.DesafioApplication;
import br.com.saulomiranda.desafio.model.Repository;
import br.com.saulomiranda.desafio.presenter.event.LoadFailed;
import br.com.saulomiranda.desafio.presenter.event.LoadListRepositorySuccess;
import br.com.saulomiranda.desafio.presenter.event.LoadMoreFailed;
import br.com.saulomiranda.desafio.repository.GithubRepository;
import br.com.saulomiranda.desafio.repository.http.CallbackHttp;
import br.com.saulomiranda.desafio.service.GithubRepositoryService;

/**
 * Created by Saulo on 25/9/16.
 */

public class GithubRepositoryServiceImpl implements GithubRepositoryService {

    private GithubRepository githubRepository;

    CallbackHttp<List<Repository>> listCallbackHttp;
    int page;

    public GithubRepositoryServiceImpl(GithubRepository githubRepository) {
        this.githubRepository = githubRepository;
        this.listCallbackHttp = getCallback();
    }

    private CallbackHttp<List<Repository>> getCallback() {

        return new CallbackHttp<List<Repository>>() {
            @Override
            public void success(List<Repository> param) {
                DesafioApplication.getInstance().providesBus().post(new LoadListRepositorySuccess(param));
            }

            @Override
            public void onError(int status, String error) {
                if(page == 1) {
                    DesafioApplication.getInstance().providesBus().post(new LoadFailed());
                } else {
                    DesafioApplication.getInstance().providesBus().post(new LoadMoreFailed());
                }

            }
        };
    }

    @Override
    public void loadGithubRepositories() {
        page = 1;
        githubRepository.getRespositories(page, listCallbackHttp);
    }

    @Override
    public void loadMoreGithubRepositories() {
        githubRepository.getRespositories(++page, listCallbackHttp);

    }
}
