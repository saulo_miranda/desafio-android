package br.com.saulomiranda.desafio;

import android.app.Application;

import com.squareup.otto.Bus;

import br.com.saulomiranda.desafio.imageLoader.ImageLoader;
import br.com.saulomiranda.desafio.imageLoader.impl.PicassoImageLoader;
import br.com.saulomiranda.desafio.repository.GithubRepository;
import br.com.saulomiranda.desafio.repository.PullRequestRepository;
import br.com.saulomiranda.desafio.repository.impl.GithubRepositoryImpl;
import br.com.saulomiranda.desafio.repository.impl.PullRequestRepositoryImpl;
import br.com.saulomiranda.desafio.service.GithubRepositoryService;
import br.com.saulomiranda.desafio.service.PullRequestService;
import br.com.saulomiranda.desafio.service.impl.GithubRepositoryServiceImpl;
import br.com.saulomiranda.desafio.service.impl.PullRequestServiceImpl;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DesafioApplication extends Application {


    private static DesafioApplication instance;

    private ImageLoader imageLoader;

    private GithubRepository githubRepository;
    private GithubRepositoryService githubRepositoryService;

    private PullRequestRepository pullRequestRepository;
    private PullRequestService pullRequestService;

    private Retrofit retrofit;
    private Bus bus;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static DesafioApplication getInstance() {
        return instance;
    }


    public ImageLoader providesImageLoader() {
        if (imageLoader == null) {
            imageLoader = new PicassoImageLoader();
        }
        return imageLoader;
    }

    public GithubRepository providesGithubRepository() {
        if (githubRepository == null) {
            githubRepository = new GithubRepositoryImpl();
        }
        return githubRepository;
    }

    public GithubRepositoryService providesGithubRepositoryService() {
        if (githubRepositoryService == null) {
            githubRepositoryService = new GithubRepositoryServiceImpl(providesGithubRepository());
        }
        return githubRepositoryService;
    }

    public Retrofit providesRetrofit() {


        if (retrofit == null) {

            retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.github.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public Bus providesBus() {
        if (bus == null) {
            bus = new Bus();
        }
        return bus;
    }


    public PullRequestRepository providesPullRequestRepository() {
        if(pullRequestRepository == null) {
            pullRequestRepository = new PullRequestRepositoryImpl();
        }
        return pullRequestRepository;
    }

    public PullRequestService providesPullRequestService() {
        if(pullRequestService == null){
            pullRequestService = new PullRequestServiceImpl(providesPullRequestRepository());
        }
        return pullRequestService;
    }
}
