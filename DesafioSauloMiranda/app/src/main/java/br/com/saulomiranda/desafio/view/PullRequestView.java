package br.com.saulomiranda.desafio.view;

import java.util.List;

import br.com.saulomiranda.desafio.model.PullRequest;

/**
 * Created by Saulo on 25/9/16..
 */

public interface PullRequestView {

    void showLoading();

    void hideLoading();

    void showPullRequests(List<PullRequest> pullRequests);

    void showError();

}
