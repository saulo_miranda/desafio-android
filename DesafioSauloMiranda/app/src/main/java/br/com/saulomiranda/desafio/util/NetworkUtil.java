package br.com.saulomiranda.desafio.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import br.com.saulomiranda.desafio.DesafioApplication;


public class NetworkUtil {

    public static boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) DesafioApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
