package br.com.saulomiranda.desafio.repository.http;

import java.util.List;

import br.com.saulomiranda.desafio.model.GitHubRequest;
import br.com.saulomiranda.desafio.model.PullRequest;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Saulo on 25/9/16.
 */

public interface GithubHttpService {

    @GET("search/repositories?q=language:Java&sorte=stars")
    Call<GitHubRequest> getRespositories(@Query(value = "page") int page);

    @GET("repos/{user}/{repo}/pulls")
    Call<List<PullRequest>> getPullRequests(
            @Path(value = "user") String user,
            @Path(value = "repo") String repo
    );


}
