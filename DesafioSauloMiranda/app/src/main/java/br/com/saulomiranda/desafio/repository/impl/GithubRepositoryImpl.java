package br.com.saulomiranda.desafio.repository.impl;

import java.util.List;

import br.com.saulomiranda.desafio.DesafioApplication;
import br.com.saulomiranda.desafio.model.GitHubRequest;
import br.com.saulomiranda.desafio.model.Repository;
import br.com.saulomiranda.desafio.repository.GithubRepository;
import br.com.saulomiranda.desafio.repository.http.CallbackHttp;
import br.com.saulomiranda.desafio.repository.http.GithubHttpService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Saulo on 25/9/16.
 */

public class GithubRepositoryImpl implements GithubRepository {

    @Override
    public void getRespositories(int page, final CallbackHttp<List<Repository>> callbackHttp) {

        GithubHttpService httpService = DesafioApplication.getInstance().providesRetrofit().create(GithubHttpService.class);

        Call<GitHubRequest> respositories = httpService.getRespositories(page);

        respositories.enqueue(new Callback<GitHubRequest>() {
            @Override
            public void onResponse(Call<GitHubRequest> call, Response<GitHubRequest> response) {
                if(response.isSuccessful()) {
                    GitHubRequest gitHubRequest = response.body();
                    callbackHttp.success(gitHubRequest.items);
                } else {
                    callbackHttp.onError(response.code(), response.message());
                }
            }

            @Override
            public void onFailure(Call<GitHubRequest> call, Throwable t) {
                    callbackHttp.onError(500, t.getMessage());
            }
        });

    }
}
