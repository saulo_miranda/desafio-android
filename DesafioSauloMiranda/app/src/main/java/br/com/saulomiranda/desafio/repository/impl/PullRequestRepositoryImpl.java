package br.com.saulomiranda.desafio.repository.impl;

import android.util.Log;

import java.util.List;

import br.com.saulomiranda.desafio.DesafioApplication;
import br.com.saulomiranda.desafio.model.PullRequest;
import br.com.saulomiranda.desafio.repository.PullRequestRepository;
import br.com.saulomiranda.desafio.repository.http.CallbackHttp;
import br.com.saulomiranda.desafio.repository.http.GithubHttpService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Saulo on 25/9/16..
 */

public class PullRequestRepositoryImpl implements PullRequestRepository {

    @Override
    public void getPullRequests(String name, String repository, final CallbackHttp<List<PullRequest>> callbackHttp) {

        GithubHttpService httpService = DesafioApplication.getInstance().providesRetrofit().create(GithubHttpService.class);

        Call<List<PullRequest>> pullRequests = httpService.getPullRequests(name, repository);

        pullRequests.enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {

                if (response.isSuccessful()) {
                    Log.i("teste", "successo");
                    callbackHttp.success(response.body());
                } else {
                    Log.i("teste", "error aqui");
                    Log.i("teste", "error aqui" + response.code());
                    Log.i("teste", "error aqui" + response.message());
                    Log.i("teste", "error aqui" + call.request().url());
                    callbackHttp.onError(response.code(), response.message());
                }
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                Log.i("teste", "error 500");
                callbackHttp.onError(500, t.getMessage());
            }
        });

    }
}
