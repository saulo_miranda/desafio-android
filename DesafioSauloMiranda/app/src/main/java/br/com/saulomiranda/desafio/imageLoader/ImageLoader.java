package br.com.saulomiranda.desafio.imageLoader;

import android.widget.ImageView;

/**
 * Created by Saulo on 25/9/16.
 */

public interface ImageLoader {

    void load(String url, ImageView imageView);

    void load(String url, ImageView imageView, int placeHolder);

    void load(String url, ImageView imageView, int width, int height);

    void load(String url, ImageView imageView, int placeholder, int width, int height);

}
